let  myGrades = [98, 95, 75, 99, 100]

Object?
	- is a collection of related data and/or functionality.
		- which usually consists of several variables and functions 
			-which are called properties and methods when they are inside objects. 

cellphone - an object 
properties(variables):
	-color 
	-weight
	-unit/model
	-brand
functions(methods):
	-alarm
	-open
	-close
	-ringing
	-send messages

Creating an object in JavaScript
	1. let cellphone = Object();
	2. let cellphone = {};

Accessing properties and methods of an objects
	- To access the properties and methods we will use the dot notation 

syntax: 
	object.properties
	object.methods()

Example:
	cellphone.color
	cellphone.ringing()

Re-assigning/Editting 
	- to re-assign values of properties and methods just use the assignment operator (=)

	Ex. 
		cellphone.color = 'Blue'

Delete properties or methods of an object
	- to delete use the keyword "delete"

	Ex. 
		delete cellphone.color - properties 
		delete cellphone.ring - methods

Object constructor 
	- to solve our problem in creating multiple object 
	- is a function that initialiazes an object