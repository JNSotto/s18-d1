//create object 
let cellphone = {
	color: 'black', 
	weight: '115 grams', 
	model: 'IPhoneX',
	brand:'Apple',
	ring:function(){
		console.log("Cellphone is ringing.");
	}
};
console.log(cellphone);

let car = Object(); 
car.color = 'red';
car.model = 'vios 2021';
car.drive = function(){
	console.log("Car is running.")
};
console.log(car);


//Re-assigning properties and method
cellphone.color = 'blue';
console.log(cellphone.color);
cellphone.ring = function(){
	console.log('The blue cellphone is ringing');
}
console.log(cellphone.ring);

//To delete properties and methods
delete cellphone.color 
console.log(cellphone);
delete cellphone.ring 
console.log(cellphone);

/*
 Object constructor
 Case: 
 	Imagine we are going to create a pokemon with multiple properties
 		-name
 		-element 
 		-level
 		-health
 		-attack 
 		-action
*/

/*let pokemon = {
	name: 'Pikachu',
 	element:'electric',
 	level:3,
 	health:100,
 	attack:50,
 	action: function(){
 		console.log('This pokomen attack target pokemon');
 		console.log("Target Pokemon's health is now reduced")
 	}
}
console.log(pokemon);*/


//Object Constructor
/*function Pokemon(name,level,element){
	this.name = name;
	this.level = level;
	this.element = element;
 	this.health = 100 + (2 * level);
 	this.attack = level * 1.25;
 	this.action = function(){
 		console.log(this.name + 'tackled target Pokemon');
 		console.log("Target Pokemon's health is now reduced")
 	}
 	this.die = function(){
 		console.log(this.name + 'died.')
 	}
}

let pikachu = new Pokemon('Pikachu', 16, 'Electric');
let ratata = new Pokemon('Ratata', 8,'Normal');
console.log(pikachu);
console.log(ratata);*/

function Pokemon(name){
	this.name = name; 
	this.health = 100; 
	this.attack = function(target){
		console.log(`${this.name} attack ${target.name}`);
		target.health -= 10;
		console.log(`${this.name}'s health is now ${target.health}`)
	}
}

let Pikachu = new Pokemon("Pikachu");
let Geodude = new Pokemon("Geodude");

Pikachu.attack(Geodude);
